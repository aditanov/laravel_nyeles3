<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use Carbon\Carbon;

class admin extends Controller
{


    function index () {
        $webinar = DB::table('course')
               ->get();
        
//            echo $webinar;
       return view('Webinar',['webinar' => $webinar]);
        }


        function inputWebinar (Request $request) {
            $file = $request->file('file');
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png' // Only allow .jpg, .bmp and .png file types.
            ]);
             
            $timestamp = strtotime($request->waktu);
           $new_date = date("d-m-Y H:i:s", $timestamp);
          $new_date1 = Carbon::create($new_date);
        
          
        DB::table('course')->insert([
            'topic_webinar' => $request->topic,
            'image_path' => $file->getClientOriginalName(),
            'speaker' => $request->speaker,
            'waktu' => $new_date1,
            'price' => $request->price
        ]);

     
        $file->move(public_path('gambar_webinar'), $file->getClientOriginalName());


        $webinar = DB::table('course')
        ->get();
 
//            echo $webinar;
return view('Webinar',['webinar' => $webinar]);
       

            }


    function webinar () {
        $webinar2 = DB::table('course')
               ->get();
    
               
        $webinar = DB::select('select a.topic_webinar,a.id_webinar,a.speaker,a.waktu,
                               (SELECT COUNT(*) FROM enrollment_transaction as b
                                where a.id_webinar = b.id_webinar) as jumlah from
                                course as a');

//        var_dump($webinar2);

//            echo $webinar;
       return view('Webinar',['webinar' => $webinar]);

    

        }

    function participant ($id_webinar) {

    $participant_join = DB::select('select * from course as a
                                    join enrollment_transaction as b 
                                    on a.id_webinar = b.id_webinar 
                                    join participant as c 
                                    on b.id_peserta = c.id_peserta 
                                    where a.id_webinar = '.$id_webinar.'');


    $participant_join = json_decode(json_encode($participant_join), true);

   
                                
    
    if (empty($participant_join)==0)
    {
        echo "ada";
        $participant_join=$participant_join;
    }
    else
    {
        echo "kosong";
        $participant_join = DB::select('select * from course as a
                                    where a.id_webinar = '.$id_webinar.'');

                                    $resultArray = json_decode(json_encode($participant_join), true);
                                    $nambaharray = array("id_peserta" => "", 
                                    "detail_pembayaran" => "", 
                                    "nama" => "", 
                                    "wassap" => "",
                                    "detail_pembayaran" => "kosong"
                                     );
                                    $participant_join[0] = array_merge($resultArray[0], $nambaharray);

    }
//print("<pre>".print_r($participant_join,true)."</pre>");
        return view('Participants',['participant_join' => $participant_join]);
        }
    
}
