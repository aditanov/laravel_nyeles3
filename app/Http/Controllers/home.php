<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class home extends Controller
{
    function utama () {
        $all = DB::table('course')
               ->orderByRaw('id_webinar DESC')
               ->limit(3)
               ->get();
     
       return view('Home',['all' => $all]);
        }

        function detail ($id,$status_pembayaran) {
          $detail_course = DB::table('course')
                ->where('id_webinar', '=', $id)
                 ->get();
         return view('events_detail2',['detail_course' => $detail_course])->with('status',$status_pembayaran);  
          }
}
