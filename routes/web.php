<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', [App\Http\Controllers\home::class, 'utama']);


Route::get('/adminw', [App\Http\Controllers\admin::class, 'webinar']);

Route::get('/adminp/{id_webinar}', [App\Http\Controllers\admin::class, 'participant']);

Route::post('/adminw/form', [App\Http\Controllers\admin::class, 'inputWebinar']);


//-------------------------------------

Route::get('/detail/{id}/{status_pembayaran}', [App\Http\Controllers\home::class, 'detail']);

Route::get('/enroll/{id}', [App\Http\Controllers\bayarcontroler::class, 'inputData']);

Route::post('/enroll/payment', [App\Http\Controllers\bayarcontroler::class, 'enroll_course']);


//batas terting--------------------------------------------------


//Route::get('/enroll', [App\Http\Controllers\home::class, 'Enrollment_webinar']);

/*Route::get('enroll', function () {
	return view('Enrollment_webinar');
});
*/
//Route::get('user', admin::class);

Route::get('detailx', function () {
	return view('events_detail2');
});

Route::get('homex', function () {
	//return view('Home');
});



Route::get('participant', function () {
	return view('participants');
});

Route::get('/home2', [App\Http\Controllers\bikin::class, 'getBalance']);

Route::get('/charge', [App\Http\Controllers\bikin::class, 'charge']);

Route::get('/charge2', [App\Http\Controllers\bikin::class, 'charge2']);

Route::get('/invoice', [App\Http\Controllers\bikin::class, 'invoice']);

Route::get('kirim-email',[App\Http\Controllers\MailController::class, 'index']);


Route::get('viewemail', function () {
    return view('emails.testing');
});