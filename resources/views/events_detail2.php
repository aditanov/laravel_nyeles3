<!DOCTYPE html>
<html style="font-size: 16px;">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Learn Everyday, Join online courses today, Train Your Brain Today!, Learn to enjoyevery minute of your life., Online Learning, Innovations in Online Learning, Education and Learning, 01, 02, 03, 04, Contact Us, INTUITIVE">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Home</title>
    <link rel="stylesheet" href="/css/nicepage.css" media="screen">

    <link rel="stylesheet" href="/css/Home.css" media="screen">

    <script class="u-script" type="text/javascript" src="/js/jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="/js/nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.9.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Oswald:200,300,400,500,600,700|Archivo+Black:400">

    <link rel="stylesheet" href="/css/ionicons.min.css">
    <link rel="stylesheet" href="/css/modal_on_detail.css">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js">



    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "",
            "url": "index.html",
            "logo": "/img/sasadasdqw.jpg"
        }
    </script>
    <meta property="og:title" content="Home">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">

    <style type="text/css">
        body {
            background-color: #eee;
        }


        .card {

            box-shadow: 0px 0px 30px 0px rgb(198, 169, 223);
            transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12);
            border: 0;
            border-radius: 1rem
        }



        .card h5 {
            overflow: hidden;
            height: 20px;
            font-weight: 900;
            font-size: 1rem
        }

        .card-img-top {
            width: 100%;
            max-height: 390px;
            object-fit: contain;
            margin-top: 20px;

        }


        .btn-warning {
            background: none rgb(198, 169, 223);
            color: #ffffff;
            fill: #ffffff;
            border: none;
            text-decoration: none;
            outline: 0;
            box-shadow: -1px 6px 19px rgb(198, 169, 223);
            border-radius: 100px
        }

        .btn-warning:hover {
            background: none rgb(198, 169, 223);
            color: #ffffff;
            box-shadow: -1px 6px 13px rgb(198, 169, 223)
        }

        .bg-success {
            font-size: 1rem;
            background-color: rgb(198, 169, 223) !important
        }
    </style>


</head>

<body class="u-body">
    <nav class="navbar navbar-expand-sm navbar-dark bg-white p-0">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/img/logo.png" alt="" width="150" height="90">
            </a>
            <button class="navbar-toggler" data-toglgle="collapse" data-target="#navbarCollapse">
                <span class="navbar-tiggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <ul class="navbar-nav ml-auto">
                    <li class="navbar-item px-2">
                        <a href="index.html" class="nav-link active text-dark">Events</a>
                    </li>
                    <li class="navbar-item px-2">
                        <a href="Users.html" class="nav-link active text-dark">Get Your Involvement</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->

        <!-- Portfolio Item Row -->
        <div class="row">
            <!-- /.row -->

            <div class="col-md-12">
                <div class="card">


                    <div class="row">
                        
                    <div class="col-md-8" >
                            <h3 class="my-3" style="margin-left: 30px;  font-weight: bold; "><?php echo  $detail_course[0]->topic_webinar; ?></h3> 
                            
                            
                          </div>
                    <div class="col-md-4" >   
                    <h4 class="my-3" style="margin-left: 30px; text-align: center; font-weight: bold; "><?php echo "Rp ".number_format($detail_course[0]->price); ?></h4> 
                        <div class="d-grid gap-2 my-4" style=" text-align: center; padding-left: 40px;" > <a href="<?php echo '/enroll/'. $detail_course[0]->id_webinar; ?>" class="btn btn-warning">Daftar Webinar</a> </div>
                    </div>

                    <div class="text-center col-md-12">
                            <img class="img-fluid" style="padding: 20px 40px 80px 40px;"  src="<?php echo '/gambar_webinar/' . $detail_course[0]->image_path; ?>" alt="">
                        </div>
                    </div>



                    <!--
                        style="margin: 20px 0px 80px 0px;"
                        <img src="<?php echo '/gambar_webinar/' . $detail_course[0]->image_path; ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <div class="clearfix mb-3"> <span class="float-start badge rounded-pill bg-success"><?php echo  $detail_course[0]->topic_webinar; ?></span> </div>
                            <h5 class="card-title"><?php echo 'Pembicara : ' . $detail_course[0]->speaker; ?></h5>
                            <h5 class="card-title"><?php echo 'Jadwal : ' . $detail_course[0]->waktu; ?></h5>
                            <div class="d-grid gap-2 my-4"> <a href="#" class="btn btn-warning">Daftar Webinar</a> </div>
                        </div>
                        -->
                </div>
            </div>


        </div>
    </div>

    </div>
    <!-- /.container -->

    <br>
    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-9fb3">
        <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
            <p class="u-small-text u-text u-text-variant u-text-1">Copyrights Nyeles Bareng Sherly</p>
        </div>
    </footer>
    <section class="u-backlink u-clearfix u-grey-80">
        <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
            <span>Website Templates</span>
        </a>
        <p class="u-text">
            <span>created with</span>
        </p>
        <a class="u-link" href="https://nicepage.com/" target="_blank">
            <span>Website Builder Software</span>
        </a>.
    </section>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header ftco-degree-bg">
		        <button type="button" class="close d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true" class="ion-ios-close"></span>
		        </button>
		      </div>
		      <div class="modal-body pt-md-0 pb-md-5 text-center">
		      	<h2>Pembayaran Anda Berhasil</h2>
		      <br>
              <br>
              <br>
		      	<h4 class="mb-2">Mohon Periksa Email Anda</h4>
		      	<h3>johndoe@gmail.com</h3>


                <br>
              <br>

              <a href="/home" class="nav-link active text-dark">Kembali</a>

		      </div>
		    </div>
		  </div>
		</div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    var x =<?php echo $status; ?>;
    if(x == 1)
    {
    $('#exampleModalCenter').modal('show');
    }
</script>
</body>

</html>