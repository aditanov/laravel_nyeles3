<!DOCTYPE html>
<html style="font-size: 16px;">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords"
        content="Learn Everyday, Join online courses today, Train Your Brain Today!, Learn to enjoyevery minute of your life., Online Learning, Innovations in Online Learning, Education and Learning, 01, 02, 03, 04, Contact Us, INTUITIVE">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Home</title>
    <link rel="stylesheet" href="/css/nicepage.css" media="screen">

    <link rel="stylesheet" href="/css/Home.css" media="screen">

    <script class="u-script" type="text/javascript" src="/js/jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="/js/nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.9.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Oswald:200,300,400,500,600,700|Archivo+Black:400">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
        integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js">



    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "",
        "url": "index.html",
        "logo": "/img/sasadasdqw.jpg"
    }
    </script>
    <meta property="og:title" content="Home">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">

    <style type="text/css">
    body {
        background-color: #eee;
    }


    .card {

        box-shadow: 0px 0px 30px 0px rgb(198, 169, 223);
        transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12);
        border: 0;
        border-radius: 1rem
    }



    .card h5 {
        overflow: hidden;
        height: 20px;
        font-weight: 900;
        font-size: 1rem
    }

    .card-img-top {
        width: 100%;
        max-height: 390px;
        object-fit: contain;
        margin-top: 20px;

    }


    .btn-warning {
        background: none rgb(198, 169, 223);
        color: #ffffff;
        fill: #ffffff;
        border: none;
        text-decoration: none;
        outline: 0;
        box-shadow: -1px 6px 19px rgb(198, 169, 223);
        border-radius: 100px
    }

    .btn-warning:hover {
        background: none rgb(198, 169, 223);
        color: #ffffff;
        box-shadow: -1px 6px 13px rgb(198, 169, 223)
    }

    .bg-success {
        font-size: 1rem;
        background-color: rgb(198, 169, 223) !important
    }


    modal {
  border-radius: 7px;
  overflow: hidden;
  background-color: transparent; }
  .modal .logo a img {
    width: 30px; }
  .modal .modal-content {
    background-color: transparent;
    border: none;
    border-radius: 7px; }
    .modal .modal-content .modal-body {
      border-radius: 7px;
      overflow: hidden;
      background-color: #fff;
      padding-left: 0px;
      padding-right: 0px;
      -webkit-box-shadow: 0 10px 50px -10px rgba(0, 0, 0, 0.9);
      box-shadow: 0 10px 50px -10px rgba(0, 0, 0, 0.9); }
      .modal .modal-content .modal-body h2 {
        font-size: 18px; }
      .modal .modal-content .modal-body p {
        color: #777;
        font-size: 14px; }
      .modal .modal-content .modal-body h3 {
        color: #000;
        font-size: 22px; }
      .modal .modal-content .modal-body .close-btn {
        color: #000; }
      .modal .modal-content .modal-body .promo-img {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%; }
        .modal .modal-content .modal-body .promo-img .price {
          top: 20px;
          left: 20px;
          position: absolute;
          color: #fff; }
  .modal .btn {
    border-radius: 30px; }
  .modal .warp-icon {
    width: 80px;
    height: 80px;
    margin: 0 auto;
    position: relative;
    background: rgba(62, 100, 255, 0.05);
    color: #3e64ff;
    border-radius: 50%; }
    .modal .warp-icon span {
      font-size: 40px;
      position: absolute;
      left: 50%;
      top: 50%;
      -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%); }

.form-control {
  border: none;
  border-radius: 0;
  border-bottom: 1px solid #ccc;
  padding-left: 0;
  padding-right: 0; }
  .form-control:active, .form-control:focus, .form-control:hover {
    border-bottom: 1px solid #000;
    -webkit-box-shadow: none;
    box-shadow: none;
    outline: none; }

.btn {
  border-radius: 4px;
  border: none;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 30px;
  padding-right: 30px; }
  .btn:active, .btn:focus {
    outline: none;
    -webkit-box-shadow: none !important;
    box-shadow: none !important; }

.close-btn {
  position: absolute;
  right: 20px;
  top: 20px;
  font-size: 20px; }
  .close-btn span {
    color: #ccc; }
  .close-btn:hover span {
    color: #000; }

    </style>


</head>

<body class="u-body">
    <nav class="navbar navbar-expand-sm navbar-dark bg-white p-0">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/img/logo.png" alt="" width="150" height="90">
            </a>
            <button class="navbar-toggler" data-toglgle="collapse" data-target="#navbarCollapse">
                <span class="navbar-tiggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <ul class="navbar-nav ml-auto">
                    <li class="navbar-item px-2">
                        <a href="index.html" class="nav-link active text-dark">Events</a>
                    </li>
                    <li class="navbar-item px-2">
                        <a href="Users.html" class="nav-link active text-dark">Get Your Involvement</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->

        <!-- Portfolio Item Row -->
        <div class="row">
            <!-- /.row -->

            <div class="col-md-12">
                <div class="card">


                    <div class="row">
                        <div class="col-md-5">
                            <img class="img-fluid" style="padding: 20px 0px 20px 20px;"
                                src="<?php echo '/gambar_webinar/' . $detail_course[0]->image_path; ?>"
                                alt="">
                        </div>

                        <div class="col-md-7" style="padding: 00px 40px 80px 20px;" ;>

                            <h3 class="my-3"><?php echo $detail_course[0]->topic_webinar; ?></h3>

                            <div class="d-flex">
                                <ul class="list-unstyled">
                                    <li>
                                        <span class="text-left text-w-normal text-size-12">
                                            <i class="far fa-clock"></i> <?php echo $detail_course[0]->waktu; ?>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="text-left text-w-normal text-size-12">
                                            <i class="fas fa-map-marker-alt"></i> Online
                                        </span>
                                    </li>
                                </ul>
                                <ul class="list-unstyled pl-5" style="margin-left: 190px;">
                                    <li>
                                        <span class="text-left text-w-normal text-size-12">
                                            <i class="far fa-money-bill-alt"></i> <?php echo "Rp. ".$detail_course[0]->price; ?>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                             

                            <form method="POST" action="/enroll/payment" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama</label>
                                        <input type="text" class="form-control" id="nama" name="nama">
                                </div>
                                <div class="form-group">
                                    <label >Nomor Wassap</label>
                                    <input type="tel" id="phone" name="phone" class="form-control" placeholder="08...." required>
                                </div>
                                <input type="hidden" value="<?php echo $detail_course[0]->id_webinar; ?>" name="id_webinar" />
                                <button type="submit" class="btn btn-primary">Daftar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <br>
    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-9fb3">
        <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
            <p class="u-small-text u-text u-text-variant u-text-1">Copyrights Nyeles Bareng Sherly</p>
        </div>
    </footer>
    <section class="u-backlink u-clearfix u-grey-80">
        <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
            <span>Website Templates</span>
        </a>
        <p class="u-text">
            <span>created with</span>
        </p>
        <a class="u-link" href="https://nicepage.com/" target="_blank">
            <span>Website Builder Software</span>
        </a>.
    </section>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content rounded-0">
          <div class="modal-body p-4 px-5">

            
            <div class="main-content text-center">
                
                <a href="#" class="close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><span class="icon-close2"></span></span>
                  </a>

                <div class="warp-icon mb-4">
                  <span class="icon-lock2"></span>
                </div>
               
                  <label for="">email anda sudah terdaftar pada webinar</label>
                  <p class="mb-4">mohon masukan email lain  pada webinar yang akan didaftarkan</p>
            </div>
          </div>
        </div>
      </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        var x = <?php echo $status; ?>;
    if(x == 1)
    {
    $('#exampleModalCenter').modal('show');
    }
    </script>
</body>

</html>