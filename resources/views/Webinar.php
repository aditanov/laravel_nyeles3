<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Bootstrap Theme</title>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark p-0">
        <div class="container">
            <a href="index.html" class="navbar-brand">Nyeles bareng Sherly</a>
            <button class="navbar-toggler" data-toglgle="collapse" data-target="#navbarCollapse">
                <span class="navbar-tiggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav">
                    <li class="navbar-item px-2">
                        <a href="index.html" class="nav-link active">Events</a>
                    </li>
                    <li class="navbar-item px-2">
                        <a href="Users.html" class="nav-link active">Participants</a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown mr-3">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-user"></i> Welcome Brad
                        </a>
                        <div class="dropdown-menu">
                            <a href="profile.html" class="dropdown-item">
                                <i class="fas fa-user-circle"></i> Profile
                                <a href="settings.html" class="dropdown-item">
                                    <i class="fas fa-cog"></i> Settings
                                </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>



    <!-- HEADER -->
    <header id="main-header" class="py-2 bg-primary text-white"></header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>
                    <i class="fa fa-podcast" aria-hidden="true"></i> Events Webinar
                </h1>
            </div>
        </div>
    </div>
    </header>

    <!-- ACTIONS -->
    <section id="actions" class="py-4 mb-4 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addPostModal">
                        <i class="fas fa-plus"></i> Tambah Webinar
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- POSTS -->
    <section id="posts">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Id</th>
                                    <th>Topic</th>
                                    <th>Speaker</th>
                                    <th>Date</th>
                                    <th>Participants</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($webinar as $webinar) { ?>
                                    <tr>
                                        <td><?php echo $webinar->id_webinar ?></td>
                                        <td><a href="adminp/<?php echo $webinar->id_webinar ?>" class=""><?php echo $webinar->topic_webinar ?></a></td>
                                        <td><?php echo $webinar->speaker ?></td>
                                        <td><?php echo $webinar->waktu ?></td>
                                        <td><?php echo $webinar->jumlah ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

    </section>

    <!-- FOOTER -->
    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">
                        Copyright &copy;
                        <span id="year"></span> Nyeles Bareng Sherly
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <!-- ADD POST MODAL -->
    <div class="modal fade" id="addPostModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">Tambah Webinar</h5>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form method="POST" action="/adminw/form" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="title">Topic Webinar</label>
                            <input type="text" class="form-control" id="webinar" name="topic">
                        </div>

                        <div class="form-group">
                            <label for="title">Speaker</label>
                            <input type="text" class="form-control"name="speaker">
                        </div>


                        <div class="form-group">
                            <label for="title">Schedule</label>
                            <input id="input" width="234" name="waktu"/>
                        </div>
                        <script>
                            $('#input').datetimepicker({
                                uiLibrary: 'bootstrap4',
                                modal: true,
                                footer: true
                            });
                        </script>
                        <div class="form-group">
                            <label for="title">Harga</label>
                            <input type="number" min="0" name="price" id="broker_fees" class="form-control" required="required">
                        </div>

                        <div class="form-group">
                            <label for="image">Upload Image</label>
                            <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" required accept="image/png, image/gif, image/jpeg">   
                                <label for="image" class="custom-file-label">Choose File</label>
                            </div>
                        </div>
                        <input type="submit" name="button" id="button" value="Submit">
                    </form>

                </div>
                
            </div>
        </div>
    </div>
    <!-- end MODAL -->

    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script>
        // Get the current year for the copyright
        $('#year').text(new Date().getFullYear());
    </script>

<script>
$(document).ready(function() {
  $('input[type="file"]').on("change", function() {
    let filenames = [];
    let files = this.files;
    if (files.length > 1) {
      filenames.push("Total Files (" + files.length + ")");
    } else {
      for (let i in files) {
        if (files.hasOwnProperty(i)) {
          filenames.push(files[i].name);
        }
      }
    }
    $(this)
      .next(".custom-file-label")
      .html(filenames.join(","));
  });
});
</script>



</body>





</html>