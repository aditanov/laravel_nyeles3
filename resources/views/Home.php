
<!DOCTYPE html>
<html style="font-size: 16px;">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Learn Everyday, Join online courses today, Train Your Brain Today!, Learn to enjoyevery minute of your life., Online Learning, Innovations in Online Learning, Education and Learning, 01, 02, 03, 04, Contact Us, INTUITIVE">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Home</title>
    <link rel="stylesheet" href="/css/nicepage.css" media="screen">
    <link rel="stylesheet" href="/css/Home.css" media="screen">
    <script class="u-script" type="text/javascript" src="/js/jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="/js/nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.9.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Oswald:200,300,400,500,600,700|Archivo+Black:400">


      <!-- link to bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js">






    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "",
            "url": "index.html",
            "logo": "/img/sasadasdqw.jpg"
        }
    </script>
    <meta property="og:title" content="Home">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">
</head>

<body class="u-body">
<nav class="navbar navbar-expand-sm navbar-dark bg-white p-0">
        <div class="container">
        <a class="navbar-brand" href="#">
      <img src="/img/logo.png" alt="" width="150" height="90">
    </a>
            <button class="navbar-toggler" data-toglgle="collapse" data-target="#navbarCollapse">
                <span class="navbar-tiggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
              
                <ul class="navbar-nav ml-auto">
                <li class="navbar-item px-2">
                        <a href="/home#events" class="nav-link active text-dark">Events</a>
                    </li>
                    <li class="navbar-item px-2">
                        <a href="Users.html" class="nav-link active text-dark">Get Your Involvement</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="u-align-center u-clearfix u-custom-color-1 u-section-1" id="carousel_064f">
        <div class="u-clearfix u-sheet u-sheet-1">
            <div class="u-clearfix u-layout-wrap u-layout-wrap-1">
                <div class="u-layout">
                    <div class="u-layout-row">
                        <div class="u-align-left u-container-style u-layout-cell u-left-cell u-size-20 u-layout-cell-1">
                            <div class="u-container-layout u-container-layout-1">
                                <a href="#" class="u-border-2 u-border-grey-dark-1 u-btn u-btn-rectangle u-button-style u-none u-btn-1">learn more</a>
                                <div class="u-shape u-shape-svg u-text-white u-shape-1">
                                    <svg class="u-svg-link" preserveAspectRatio="none" viewBox="0 0 160 50" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-fca1"></use></svg>
                                    <svg class="u-svg-content" id="svg-fca1" style="enable-background:new 0 0 160 50;"><path d="M133,26.7c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,22
	c11.2-7.8,20.6-8.1,32.2,0c11,7.6,19,8.5,31.3,0c11.6-8.1,22.4-7.7,33.5,0c11.4,8,20.3,8.3,32.2,0c11.6-8.1,19.2-8.1,30.8,0
	l-3.8,4.7C146.9,20.2,142.3,20.2,133,26.7z M133,10.8c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0
	c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,6.1c11.2-7.8,20.6-8.1,32.2,0c11,7.6,19,8.5,31.3,0C75.1-2,85.9-1.6,97,6.1
	c11.4,8,20.3,8.3,32.2,0C140.8-2,148.4-2,160,6.1l-3.8,4.7C146.9,4.3,142.3,4.3,133,10.8z M32.2,38c11,7.6,19,8.5,31.3,0
	c11.6-8.1,22.4-7.7,33.5,0c11.4,8,20.3,8.3,32.2,0c11.6-8.1,19.2-8.1,30.8,0l-3.8,4.7c-9.3-6.5-13.9-6.5-23.3,0
	c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,38
	C11.2,30.2,20.6,29.9,32.2,38z"></path></svg>
                                </div>
                            </div>
                        </div>
                        <div class="u-container-style u-image u-layout-cell u-right-cell u-size-40 u-image-1" style="background-image: url(/img/fsae-min.jpg");" data-image-width="1500" data-image-height="996">
                            <div class="u-container-layout u-container-layout-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h1 class="u-text u-text-1"><b>Online Sales&nbsp;</b>
                <br>Learning Platform
            </h1>
            <p class="u-text u-text-2">Tingkatkan kemampuan anda dalam bidang sales, Marketing dan bisnis melalui metode yang interaktif</p>
        </div>
    </section>
    <section class="u-align-center u-clearfix u-section-2" id="carousel_cf92">
        <div class="u-clearfix u-sheet u-sheet-1"id="events">
            <div class="u-expanded-width u-shape u-shape-svg u-text-custom-color-1 u-shape-1">
                <svg class="u-svg-link" preserveAspectRatio="none" viewBox="0 0 160 80" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-0b7f"></use></svg>
                <svg class="u-svg-content" id="svg-0b7f" style="enable-background:new 0 0 160 80;"><path d="M160,0H0c0,44.2,35.8,80,80,80S160,44.2,160,0z"></path></svg>
            </div>
            <h1 class="u-custom-font u-text u-text-body-alt-color u-text-1">Upcoming Events</h1>
            <div class="u-expanded-width u-gallery u-layout-grid u-lightbox u-show-text-on-hover u-gallery-1">
                <div class="u-gallery-inner u-gallery-inner-1">
                <?php
            foreach ($all as $all) { ?>
                <div class="u-effect-fade u-gallery-item">
                        <div class="u-back-slide" data-image-width="512" data-image-height="510">
            <a href="<?php echo '/detail/'.$all->id_webinar.'/0'; ?>">
                        <img class="u-back-image u-expanded" src="<?php echo '/gambar_webinar/'.$all->image_path; ?>">   
                        </a>    
                    </div>
                        
                    </div>
                   <?php }
                ?>   
                </div>
            </div>
        </div>
    </section>
    <section class="u-clearfix u-custom-color-1 u-section-3" id="carousel_ed88">
        <div class="u-clearfix u-sheet u-valign-middle-lg u-valign-middle-md u-valign-middle-xl u-valign-middle-xs u-sheet-1">
            <div class="u-clearfix u-expanded-width u-gutter-22 u-layout-wrap u-layout-wrap-1">
                <div class="u-gutter-0 u-layout">
                    <div class="u-layout-row">
                        <div class="u-container-style u-custom-color-1 u-layout-cell u-shape-rectangle u-size-30 u-layout-cell-1">
                            <div class="u-container-layout u-container-layout-1">
                                <div class="u-shape u-shape-svg u-text-white u-shape-1">
                                    <svg class="u-svg-link" preserveAspectRatio="none" viewBox="0 0 160 50" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-6f73"></use></svg>
                                    <svg class="u-svg-content" id="svg-6f73" style="enable-background:new 0 0 160 50;"><path d="M133,26.7c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,22
	c11.2-7.8,20.6-8.1,32.2,0c11,7.6,19,8.5,31.3,0c11.6-8.1,22.4-7.7,33.5,0c11.4,8,20.3,8.3,32.2,0c11.6-8.1,19.2-8.1,30.8,0
	l-3.8,4.7C146.9,20.2,142.3,20.2,133,26.7z M133,10.8c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0
	c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,6.1c11.2-7.8,20.6-8.1,32.2,0c11,7.6,19,8.5,31.3,0C75.1-2,85.9-1.6,97,6.1
	c11.4,8,20.3,8.3,32.2,0C140.8-2,148.4-2,160,6.1l-3.8,4.7C146.9,4.3,142.3,4.3,133,10.8z M32.2,38c11,7.6,19,8.5,31.3,0
	c11.6-8.1,22.4-7.7,33.5,0c11.4,8,20.3,8.3,32.2,0c11.6-8.1,19.2-8.1,30.8,0l-3.8,4.7c-9.3-6.5-13.9-6.5-23.3,0
	c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,38
	C11.2,30.2,20.6,29.9,32.2,38z"></path></svg>
                                </div>
                                <h1 class="u-align-left u-text u-text-1">Learn Become&nbsp;<br>Great Sales
                                </h1>
                                <p class="u-align-center u-text u-text-2">Make the deals And help your costomer increase their busines</p>
                            </div>
                        </div>
                        <div class="u-container-style u-layout-cell u-size-30 u-layout-cell-2">
                            <div class="u-container-layout u-container-layout-2" src="">
                                <div class="u-align-center u-image u-image-circle u-image-1" style="background-image: url(/img/sales-consultant.jpg)" data-image-width="909" data-image-height="609"></div>
                                <div class="u-shape u-shape-svg u-text-white u-shape-2">
                                    <svg class="u-svg-link" preserveAspectRatio="none" viewBox="0 0 160 50" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-6f73"></use></svg>
                                    <svg class="u-svg-content" id="svg-6f73" style="enable-background:new 0 0 160 50;"><path d="M133,26.7c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,22
	c11.2-7.8,20.6-8.1,32.2,0c11,7.6,19,8.5,31.3,0c11.6-8.1,22.4-7.7,33.5,0c11.4,8,20.3,8.3,32.2,0c11.6-8.1,19.2-8.1,30.8,0
	l-3.8,4.7C146.9,20.2,142.3,20.2,133,26.7z M133,10.8c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0
	c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,6.1c11.2-7.8,20.6-8.1,32.2,0c11,7.6,19,8.5,31.3,0C75.1-2,85.9-1.6,97,6.1
	c11.4,8,20.3,8.3,32.2,0C140.8-2,148.4-2,160,6.1l-3.8,4.7C146.9,4.3,142.3,4.3,133,10.8z M32.2,38c11,7.6,19,8.5,31.3,0
	c11.6-8.1,22.4-7.7,33.5,0c11.4,8,20.3,8.3,32.2,0c11.6-8.1,19.2-8.1,30.8,0l-3.8,4.7c-9.3-6.5-13.9-6.5-23.3,0
	c-13.9,9.7-25.8,9.7-39.8,0c-9.1-6.3-16.8-6.3-25.9,0c-13.8,9.6-25.1,9.6-38.9,0c-9.2-6.4-15.4-6.4-24.6,0L0,38
	C11.2,30.2,20.6,29.9,32.2,38z"></path></svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="u-clearfix u-white u-section-4" id="carousel_78cb">
        <div class="u-clearfix u-sheet u-valign-middle-md u-valign-middle-sm u-sheet-1">
            <div class="u-align-center u-container-style u-custom-color-1 u-expanded-width-xs u-group u-group-1">
                <div class="u-container-layout u-container-layout-1">
                    <h2 class="u-custom-font u-font-oswald u-text u-text-1">Contact Us</h2>
                    <div class="u-align-center-sm u-align-center-xs u-align-left-lg u-align-left-md u-align-left-xl u-form u-form-1">
                        <form action="#" method="POST" class="u-clearfix u-form-spacing-28 u-form-vertical u-inner-form" style="padding: 10px" source="custom" name="form">
                            <div class="u-form-group u-form-name u-form-group-1">
                                <label for="name-5a14" class="u-form-control-hidden u-label" wfd-invisible="true">Name</label>
                                <input type="text" placeholder="Enter your Name" id="name-5a14" name="name" class="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-white" required="">
                            </div>
                            <div class="u-form-email u-form-group u-form-group-2">
                                <label for="email-5a14" class="u-form-control-hidden u-label" wfd-invisible="true">Email</label>
                                <input type="email" placeholder="Enter a valid email address" id="email-5a14" name="email" class="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-white" required="">
                            </div>
                            <div class="u-form-group u-form-message u-form-group-3">
                                <label for="message-5a14" class="u-form-control-hidden u-label" wfd-invisible="true">Message</label>
                                <textarea placeholder="Enter your message" rows="4" cols="50" id="message-5a14" name="message" class="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-white" required=""></textarea>
                            </div>
                            <div class="u-align-center u-form-group u-form-submit u-form-group-4">
                                <a href="#" class="u-border-2 u-border-black u-btn u-btn-submit u-button-style u-hover-black u-none u-text-black u-text-hover-white u-btn-1">Submit</a>
                                <input type="submit" value="submit" class="u-form-control-hidden" wfd-invisible="true">
                            </div>
                            <div class="u-form-send-message u-form-send-success" wfd-invisible="true"> Thank you! Your message has been sent. </div>
                            <div class="u-form-send-error u-form-send-message" wfd-invisible="true"> Unable to send your message. Please fix errors then try again. </div>
                            <input type="hidden" value="" name="recaptchaResponse" wfd-invisible="true">
                        </form>
                    </div>
                </div>
            </div>
            <img src="/img/rty-min.jpg" alt="" class="u-expanded-width-xs u-image u-image-default u-image-1" data-image-width="1382" data-image-height="922">
            <div class="u-list u-repeater u-list-1">
                <div class="u-container-style u-list-item u-palette-5-dark-3 u-repeater-item u-list-item-1">
                    <div class="u-container-layout u-similar-container u-container-layout-2"><span class="u-icon u-icon-circle u-text-white u-icon-1"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 513.64 513.64" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-0e47"></use></svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" xml:space="preserve" class="u-svg-content" viewBox="0 0 513.64 513.64" x="0px" y="0px" id="svg-0e47" style="enable-background:new 0 0 513.64 513.64;"><g><g><path d="M499.66,376.96l-71.68-71.68c-25.6-25.6-69.12-15.359-79.36,17.92c-7.68,23.041-33.28,35.841-56.32,30.72    c-51.2-12.8-120.32-79.36-133.12-133.12c-7.68-23.041,7.68-48.641,30.72-56.32c33.28-10.24,43.52-53.76,17.92-79.36l-71.68-71.68    c-20.48-17.92-51.2-17.92-69.12,0l-48.64,48.64c-48.64,51.2,5.12,186.88,125.44,307.2c120.32,120.32,256,176.641,307.2,125.44    l48.64-48.64C517.581,425.6,517.581,394.88,499.66,376.96z"></path>
</g>
</g></svg></span>
                        <h5 class="u-text u-text-2">Call Us</h5>
                        <p class="u-text u-text-3">+62 868975735</p>
                    </div>
                </div>
                <div class="u-container-style u-list-item u-palette-5-dark-3 u-repeater-item u-list-item-2">
                    <div class="u-container-layout u-similar-container u-container-layout-3"><span class="u-icon u-icon-circle u-text-white u-icon-2"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-94ed"></use></svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" x="0px" y="0px" id="svg-94ed" style="enable-background:new 0 0 512 512;"><g><g><path d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5    c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021    C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333    s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z"></path>
</g>
</g></svg></span>
                        <h5 class="u-text u-text-4">Location</h5>
                        <p class="u-text u-text-5">Co-Hive Kebon Jeruk,&nbsp;West Jakarta</p>
                    </div>
                </div>
                <div class="u-container-style u-list-item u-palette-5-dark-3 u-repeater-item u-list-item-3">
                    <div class="u-container-layout u-similar-container u-container-layout-4"><span class="u-icon u-icon-circle u-text-white u-icon-3"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-c080"></use></svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" id="svg-c080"><g><path d="m386.058 256c0-8.284 6.716-15 15-15h31.587c-7.224-85.814-75.831-154.421-161.645-161.645v31.588c0 8.284-6.716 15-15 15s-15-6.716-15-15v-31.588c-85.814 7.224-154.421 75.831-161.645 161.645h31.587c8.284 0 15 6.716 15 15s-6.716 15-15 15h-31.587c7.224 85.814 75.831 154.421 161.645 161.645v-31.588c0-8.284 6.716-15 15-15s15 6.716 15 15v31.588c85.814-7.224 154.421-75.831 161.645-161.645h-31.587c-8.284 0-15-6.716-15-15zm-39.467-71.629-79.838 82.087c-5.558 5.714-14.618 6.086-20.625.835l-59.598-52.101c-6.237-5.452-6.873-14.929-1.42-21.165 5.452-6.237 14.928-6.875 21.166-1.421l48.889 42.739 69.921-71.891c5.776-5.938 15.273-6.069 21.211-.295 5.938 5.778 6.07 15.274.294 21.212z"></path><path d="m256 0c-141.159 0-256 114.841-256 256s114.841 256 256 256 256-114.841 256-256-114.841-256-256-256zm0 463.286c-114.298 0-207.286-92.988-207.286-207.286s92.988-207.286 207.286-207.286 207.286 92.988 207.286 207.286-92.988 207.286-207.286 207.286z"></path>
</g></svg></span>
                        <h5 class="u-text u-text-6">Business Hours</h5>
                        <p class="u-text u-text-7">Mon – Fri …… 10 am – 8 pm, Sat, Sun ....… Closed</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-9fb3">
        <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
            <p class="u-small-text u-text u-text-variant u-text-1">Copyrights Nyeles Bareng Sherly</p>
        </div>
    </footer>
    <section class="u-backlink u-clearfix u-grey-80">
        <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
            <span>Website Templates</span>
        </a>
        <p class="u-text">
            <span>created with</span>
        </p>
        <a class="u-link" href="https://nicepage.com/" target="_blank">
            <span>Website Builder Software</span>
        </a>.
    </section>
</body>

</html>